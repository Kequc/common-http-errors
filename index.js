const Errors = {
    BadGateway: require('./errors/bad-gateway'),
    BadRequest: require('./errors/bad-request'),
    BandwidthLimitExceeded: require('./errors/bandwidth-limit-exceeded'),
    BlockedByWindowsParentalControls: require('./errors/blocked-by-windows-parental-controls'),
    ClientClosedRequest: require('./errors/client-closed-request'),
    Conflict: require('./errors/conflict'),
    EnhanceYourCalm: require('./errors/enhance-your-calm'),
    ExpectationFailed: require('./errors/expectation-failed'),
    FailedDependency: require('./errors/failed-dependency'),
    Forbidden: require('./errors/forbidden'),
    GatewayTimeout: require('./errors/gateway-timeout'),
    Gone: require('./errors/gone'),
    HttpVersionNotSupported: require('./errors/http-version-not-supported'),
    IAmATeapot: require('./errors/i-am-a-teapot'),
    InsufficientStorage: require('./errors/insufficient-storage'),
    InternalServerError: require('./errors/internal-server-error'),
    LengthRequired: require('./errors/length-required'),
    Locked: require('./errors/locked'),
    LoopDetected: require('./errors/loop-detected'),
    MethodNotAllowed: require('./errors/method-not-allowed'),
    NetworkAuthenticationRequired: require('./errors/network-authentication-required'),
    NetworkConnectTimeoutError: require('./errors/network-connect-timeout-error'),
    NetworkReadTimeoutError: require('./errors/network-read-timeout-error'),
    NoResponse: require('./errors/no-response'),
    NotAcceptable: require('./errors/not-acceptable'),
    NotExtended: require('./errors/not-extended'),
    NotFound: require('./errors/not-found'),
    NotImplemented: require('./errors/not-implemented'),
    PreconditionFailed: require('./errors/precondition-failed'),
    PreconditionRequired: require('./errors/precondition-required'),
    ProxyAuthenticationRequired: require('./errors/proxy-authentication-required'),
    RequestEntityTooLarge: require('./errors/request-entity-too-large'),
    RequestHeaderFieldsTooLarge: require('./errors/request-header-fields-too-large'),
    RequestTimeout: require('./errors/request-timeout'),
    RequestUriTooLong: require('./errors/request-uri-too-long'),
    RequestedRangeNotSatisfiable: require('./errors/requested-range-not-satisfiable'),
    ReservedForWebdav: require('./errors/reserved-for-webdav'),
    RetryWith: require('./errors/retry-with'),
    ServiceUnavailable: require('./errors/service-unavailable'),
    TooManyRequests: require('./errors/too-many-requests'),
    Unauthorized: require('./errors/unauthorized'),
    UnprocessableEntity: require('./errors/unprocessable-entity'),
    UnsupportedMediaType: require('./errors/unsupported-media-type'),
    UpgradeRequired: require('./errors/upgrade-required'),
    VariantAlsoNegotiates: require('./errors/variant-also-negotiates')
};

Errors.forCode = function(statusCode) {
    if (typeof statusCode == 'number' && statusCode >= 400 && statusCode < 600) {
        for (const key of Object.keys(Errors)) {
            if (Errors[key].statusCode === statusCode) {
                return Errors[key];
            }
        }
    }
    return undefined;
};

module.exports = Errors;
