// This status code, while used by many servers, is not specified in any RFCs.

module.exports = function BandwidthLimitExceeded(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Bandwidth limit exceeded';
    this.statusCode = 509;
    this.errorCode = errorCode || 509;
};

require('util').inherits(module.exports, Error);
