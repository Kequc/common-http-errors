// This status code is not specified in any RFCs, but is used by some HTTP proxies to signal a
// network read timeout behind the proxy to a client in front of the proxy.

module.exports = function NetworkReadTimeoutError(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Network read timeout error';
    this.statusCode = 598;
    this.errorCode = errorCode || 598;
};

require('util').inherits(module.exports, Error);
