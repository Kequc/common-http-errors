// The request could not be understood by the server due to malformed syntax. The client SHOULD
// NOT repeat the request without modifications.

module.exports = function BadRequest(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Bad request';
    this.statusCode = 400;
    this.errorCode = errorCode || 400;
};

require('util').inherits(module.exports, Error);
