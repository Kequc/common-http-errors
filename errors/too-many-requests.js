// The 429 status code indicates that the user has sent too many requests in a given amount of
// time (rate limiting). The response representations SHOULD include details explaining the
// condition, and MAY include a Retry-After header indicating how long to wait before making a new
// request.

module.exports = function TooManyRequests(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Too many requests';
    this.statusCode = 429;
    this.errorCode = errorCode || 429;
};

require('util').inherits(module.exports, Error);
