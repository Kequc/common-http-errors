// Network connection timeout behind the proxy to a client in front of the proxy

module.exports = function NetworkConnectTimeoutError(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Network connect timeout error';
    this.statusCode = 599;
    this.errorCode = errorCode || 599;
};

require('util').inherits(module.exports, Error);
