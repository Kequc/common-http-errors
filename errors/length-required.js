// The server refuses to accept the request without a defined Content- Length. The client MAY
// repeat the request if it adds a valid Content-Length header field containing the length of the
// message-body in the request message.

module.exports = function LengthRequired(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Length required';
    this.statusCode = 411;
    this.errorCode = errorCode || 411;
};

require('util').inherits(module.exports, Error);
