// The origin server requires the request to be conditional. Intended to prevent the LOST UPDATE
// PROBLEM, where a client GETs a resource's state, modifies it, and PUTs it back to the server,
// when meanwhile a third party has modified the state on the server, leading to a conflict.

module.exports = function PreconditionRequired(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Precondition required';
    this.statusCode = 428;
    this.errorCode = errorCode || 428;
};

require('util').inherits(module.exports, Error);
