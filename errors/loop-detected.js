// The 508 (Loop Detected) status code indicates that the server terminated an operation because
// it encountered an infinite loop while processing a request with Depth: infinity. This status
// indicates that the entire operation failed.

module.exports = function LoopDetected(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Loop detected';
    this.statusCode = 508;
    this.errorCode = errorCode || 508;
};

require('util').inherits(module.exports, Error);
