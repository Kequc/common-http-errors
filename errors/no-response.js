// The server returns no information to the client and closes the connection (useful as a
// deterrent for malware).

module.exports = function NoResponse(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'No response';
    this.statusCode = 444;
    this.errorCode = errorCode || 444;
};

require('util').inherits(module.exports, Error);
