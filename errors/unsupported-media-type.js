// The server is refusing to service the request because the entity of the request is in a format
// not supported by the requested resource for the requested method.

module.exports = function UnsupportedMediaType(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Unsupported media type';
    this.statusCode = 415;
    this.errorCode = errorCode || 415;
};

require('util').inherits(module.exports, Error);
