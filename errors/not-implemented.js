// The server does not support the functionality required to fulfill the request. This is the
// appropriate response when the server does not recognize the request method and is not capable
// of supporting it for any resource.

module.exports = function NotImplemented(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Not implemented';
    this.statusCode = 501;
    this.errorCode = errorCode || 501;
};

require('util').inherits(module.exports, Error);
