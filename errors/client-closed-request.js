// This code is introduced to log the case when the connection is closed by client while HTTP
// server is processing its request, making server unable to send the HTTP header back.

module.exports = function ClientClosedRequest(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Client closed request';
    this.statusCode = 499;
    this.errorCode = errorCode || 499;
};

require('util').inherits(module.exports, Error);
