// The 507 (Insufficient Storage) status code means the method could not be performed on the
// resource because the server is unable to store the representation needed to successfully
// complete the request. This condition is considered to be temporary. If the request that
// received this status code was the result of a user action, the request MUST NOT be repeated
// until it is requested by a separate user action.

module.exports = function InsufficientStorage(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Insufficient storage';
    this.statusCode = 507;
    this.errorCode = errorCode || 507;
};

require('util').inherits(module.exports, Error);
