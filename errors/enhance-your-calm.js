// Returned by the API endpoint when the client is being rate limited. Likely a reference to this
// number's association with marijuana. Other services may wish to implement the 429 Too Many
// Requests response code instead.

module.exports = function EnhanceYourCalm(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Enhance your calm';
    this.statusCode = 420;
    this.errorCode = errorCode || 420;
};

require('util').inherits(module.exports, Error);
