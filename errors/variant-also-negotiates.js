// The 506 status code indicates that the server has an internal configuration error: the chosen
// variant resource is configured to engage in transparent content negotiation itself, and is
// therefore not a proper end point in the negotiation process.

module.exports = function VariantAlsoNegotiates(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Variant also negotiates';
    this.statusCode = 506;
    this.errorCode = errorCode || 506;
};

require('util').inherits(module.exports, Error);
