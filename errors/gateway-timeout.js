// The server, while acting as a gateway or proxy, did not receive a timely response from the
// upstream server specified by the URI (e.g. HTTP, FTP, LDAP) or some other auxiliary server
// (e.g. DNS) it needed to access in attempting to complete the request.

module.exports = function GatewayTimeout(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Gateway timeout';
    this.statusCode = 504;
    this.errorCode = errorCode || 504;
};

require('util').inherits(module.exports, Error);
