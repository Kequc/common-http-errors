// The 431 status code indicates that the server is unwilling to process the request because its
// header fields are too large. The request MAY be resubmitted after reducing the size of the
// request header fields.

module.exports = function RequestHeaderFieldsTooLarge(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Request header fields too large';
    this.statusCode = 431;
    this.errorCode = errorCode || 431;
};

require('util').inherits(module.exports, Error);
