// The precondition given in one or more of the request-header fields evaluated to false when it
// was tested on the server. This response code allows the client to place preconditions on the
// current resource metainformation (header field data) and thus prevent the requested method from
// being applied to a resource other than the one intended.

module.exports = function PreconditionFailed(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Precondition failed';
    this.statusCode = 412;
    this.errorCode = errorCode || 412;
};

require('util').inherits(module.exports, Error);
