// The expectation given in an Expect request-header field could not be met by this server, or, if
// the server is a proxy, the server has unambiguous evidence that the request could not be met by
// the next-hop server.

module.exports = function ExpectationFailed(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Expectation failed';
    this.statusCode = 417;
    this.errorCode = errorCode || 417;
};

require('util').inherits(module.exports, Error);
