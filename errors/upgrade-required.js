// Reliable, interoperable negotiation of Upgrade features requires an unambiguous failure signal.
// The 426 Upgrade Required status code allows a server to definitively state the precise protocol
// extensions a given resource must be served with.

module.exports = function UpgradeRequired(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Upgrade required';
    this.statusCode = 426;
    this.errorCode = errorCode || 426;
};

require('util').inherits(module.exports, Error);
