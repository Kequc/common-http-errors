// The 422 (Unprocessable Entity) status code means the server understands the content type of the
// request entity (hence a 415(Unsupported Media Type) status code is inappropriate), and the
// syntax of the request entity is correct (thus a 400 (Bad Request) status code is inappropriate)
// but was unable to process the contained instructions. For example, this error condition may
// occur if an XML request body contains well-formed (i.e., syntactically correct), but
// semantically erroneous, XML instructions.

module.exports = function UnprocessableEntity(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Unprocessable entity';
    this.statusCode = 422;
    this.errorCode = errorCode || 422;
};

require('util').inherits(module.exports, Error);
