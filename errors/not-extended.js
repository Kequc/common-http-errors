// The policy for accessing the resource has not been met in the request. The server should send
// back all the information necessary for the client to issue an extended request. It is outside
// the scope of this specification to specify how the extensions inform the client.

module.exports = function NotExtended(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Not extended';
    this.statusCode = 510;
    this.errorCode = errorCode || 510;
};

require('util').inherits(module.exports, Error);
