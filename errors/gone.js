// The requested resource is no longer available at the server and no forwarding address is known.
// This condition is expected to be considered permanent. Clients with link editing capabilities
// SHOULD delete references to the Request-URI after user approval. If the server does not know,
// or has no facility to determine, whether or not the condition is permanent, the status code 404
// (Not Found) SHOULD be used instead. This response is cacheable unless indicated otherwise.

module.exports = function Gone(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Gone';
    this.statusCode = 410;
    this.errorCode = errorCode || 410;
};

require('util').inherits(module.exports, Error);
