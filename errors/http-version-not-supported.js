// The server does not support, or refuses to support, the HTTP protocol version that was used in
// the request message. The server is indicating that it is unable or unwilling to complete the
// request using the same major version as the client, as described in section 3.1, other than
// with this error message. The response SHOULD contain an entity describing why that version is
// not supported and what other protocols are supported by that server.

module.exports = function HttpVersionNotSupported(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Http version not supported';
    this.statusCode = 505;
    this.errorCode = errorCode || 505;
};

require('util').inherits(module.exports, Error);
