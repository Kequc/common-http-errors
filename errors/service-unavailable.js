// The server is currently unable to handle the request due to a temporary overloading or
// maintenance of the server. The implication is that this is a temporary condition which will be
// alleviated after some delay. If known, the length of the delay MAY be indicated in a
// Retry-After header. If no Retry-After is given, the client SHOULD handle the response as it
// would for a 500 response.

module.exports = function ServiceUnavailable(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Service unavailable';
    this.statusCode = 503;
    this.errorCode = errorCode || 503;
};

require('util').inherits(module.exports, Error);
