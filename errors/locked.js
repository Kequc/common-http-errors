// The 423 (Locked) status code means the source or destination resource of a method is locked.
// This response SHOULD contain an appropriate precondition or postcondition code, such as
// 'lock-token-submitted' or 'no-conflicting-lock'.

module.exports = function Locked(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Locked';
    this.statusCode = 423;
    this.errorCode = errorCode || 423;
};

require('util').inherits(module.exports, Error);
