// The server encountered an unexpected condition which prevented it from fulfilling the request.

module.exports = function InternalServerError(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Internal server error';
    this.statusCode = 500;
    this.errorCode = errorCode || 500;
};

require('util').inherits(module.exports, Error);
