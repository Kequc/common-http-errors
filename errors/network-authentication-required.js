// The 511 status code indicates that the client needs to authenticate to gain network access. The
// response representation SHOULD contain a link to a resource that allows the user to submit
// credentials.

module.exports = function NetworkAuthenticationRequired(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Network authentication required';
    this.statusCode = 511;
    this.errorCode = errorCode || 511;
};

require('util').inherits(module.exports, Error);
