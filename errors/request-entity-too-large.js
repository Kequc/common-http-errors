// The server is refusing to process a request because the request entity is larger than the
// server is willing or able to process. The server MAY close the connection to prevent the client
// from continuing the request.

module.exports = function RequestEntityTooLarge(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Request entity too large';
    this.statusCode = 413;
    this.errorCode = errorCode || 413;
};

require('util').inherits(module.exports, Error);
