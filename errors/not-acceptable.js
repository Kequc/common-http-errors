// The resource identified by the request is only capable of generating response entities which
// have content characteristics not acceptable according to the accept headers sent in the
// request.

module.exports = function NotAcceptable(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Not acceptable';
    this.statusCode = 406;
    this.errorCode = errorCode || 406;
};

require('util').inherits(module.exports, Error);
