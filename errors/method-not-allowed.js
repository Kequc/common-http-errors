// The method specified in the Request-Line is not allowed for the resource identified by the
// Request-URI. The response MUST include an Allow header containing a list of valid methods for
// the requested resource.

module.exports = function MethodNotAllowed(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Method not allowed';
    this.statusCode = 405;
    this.errorCode = errorCode || 405;
};

require('util').inherits(module.exports, Error);
