// A Microsoft extension. The request should be retried after performing the appropriate action.

module.exports = function RetryWith(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Retry with';
    this.statusCode = 449;
    this.errorCode = errorCode || 449;
};

require('util').inherits(module.exports, Error);
