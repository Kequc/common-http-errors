// Defined in drafts of WebDAV Advanced Collections Protocol, but not present in Web Distributed
// Authoring and Versioning (WebDAV) Ordered Collections Protocol.

module.exports = function ReservedForWebdav(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Reserved for webdav';
    this.statusCode = 425;
    this.errorCode = errorCode || 425;
};

require('util').inherits(module.exports, Error);
