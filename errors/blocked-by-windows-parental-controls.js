// A Microsoft extension. This error is given when Windows Parental Controls are turned on and are
// blocking access to the given webpage.

module.exports = function BlockedByWindowsParentalControls(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Blocked by windows parental controls';
    this.statusCode = 450;
    this.errorCode = errorCode || 450;
};

require('util').inherits(module.exports, Error);
