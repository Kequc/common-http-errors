// The client did not produce a request within the time that the server was prepared to wait. The
// client MAY repeat the request without modifications at any later time.

module.exports = function RequestTimeout(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Request timeout';
    this.statusCode = 408;
    this.errorCode = errorCode || 408;
};

require('util').inherits(module.exports, Error);
