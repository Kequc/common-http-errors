// This code was defined in 1998 as one of the traditional IETF April Fools' jokes, in RFC 2324,
// Hyper Text Coffee Pot Control Protocol, and is not expected to be implemented by actual HTTP
// servers.

module.exports = function IAmATeapot(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'I am a teapot';
    this.statusCode = 418;
    this.errorCode = errorCode || 418;
};

require('util').inherits(module.exports, Error);
