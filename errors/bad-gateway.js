// The server, while acting as a gateway or proxy, received an invalid response from the upstream
// server it accessed in attempting to fulfill the request.

module.exports = function BadGateway(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Bad gateway';
    this.statusCode = 502;
    this.errorCode = errorCode || 502;
};

require('util').inherits(module.exports, Error);
