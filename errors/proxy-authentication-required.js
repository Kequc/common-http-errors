// This code is similar to 401 (Unauthorized), but indicates that the client must first
// authenticate itself with the proxy. The proxy MUST return a Proxy-Authenticate header field
// containing a challenge applicable to the proxy for the requested resource. The client MAY
// repeat the request with a suitable Proxy-Authorization header field.

module.exports = function ProxyAuthenticationRequired(message, errorCode) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || 'Proxy authentication required';
    this.statusCode = 407;
    this.errorCode = errorCode || 407;
};

require('util').inherits(module.exports, Error);
